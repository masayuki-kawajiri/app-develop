window.addEventListener('load', init);
/**
 * 初期化
 */
var canvas;
var ctx;
var SCREEN_WIDTH = 800;
var SCREEN_HEIGHT = 600;

function init(){
  canvas = document.getElementById('maincanvas');
  ctx = canvas.getContext('2d');
  canvas.width = SCREEN_WIDTH;
  canvas.height= SCREEN_HEIGHT;
  requestAnimationFrame(update);
}

function update() {
  requestAnimationFrame(update);

  render();
}

function render() {
  // 全体をクリア
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}

var image = new Image();
image.src = 'dumyohan.png';
image.onload = function() {
  // 画像ファイルが読み込み終わった時に呼ばれる関数
}
