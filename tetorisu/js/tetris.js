var COLS = 10, ROWS = 20; // 横10,縦20マス
var board = []; // 盤面情報
var lose; // 一番上までいっちゃったかどうか
var interval; // いま操作しているブロックの形
var current; // 今操作しているブロックの形
var currentX, currentY; // 今操作しているブロックの位置

// 操作するブロックのパターン
var shapes = [
  [1,1,1,1],
  [1,1,1,0,1],
  [1,1,1,0,0,0,1],
  [1,1,0,0,1,1],
  [1,1,0,0,0,1,1],
  [0,1,1,0,1,1],
  [0,1,0,0,1,1,1]
];

var colors = [
  'cyan',
  'orange',
  'blue',
  'yellow',
  'red',
  'green',
  'purple'
];

function newGame(){
  clearInterval(interval); // ゲームタイマーをクリア
  init(); // 盤面をまっさらにする
  newShape(); // 操作ブロックをセット
  lose = false; // 負けフラッグ
  interval = setInterval( tick, 250 ); // 250秒ごとにtickという関数を呼び出す
}

newGame();
